#! /bin/bash
if [[ -e $1 ]]; then
        echo "Please specify the host ip"
        exit 1
fi

export HOST_IP=$1


docker-compose -f ./docker-compose-env-setup.yml up

