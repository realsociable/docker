#! /usr/bin/env node
if (!process.argv[2]) {
    console.error("hostname not passed.. Exiting")
    process.exit(1);
}
var fs = require("fs");
var data = fs.readFileSync("/code/realsociable/RealSociableSearchEngine/env.json");
var envJSON = JSON.parse(data);
envJSON.development.db_url = `mongodb://${process.argv[2]}:27017/real_sociable`
envJSON.development.invite_url = `http://${process.argv[2]}:3000/api/user-invites/validate/`
envJSON.development.ui_redirect_url= `http://${process.argv[2]}:8081/admin-Registration/`
envJSON.development.tokenUrls.facebook= {
        "facebookMsLongLivedAccessToken": `http://${process.argv[2]}:8080/api/ms/rs/facebook/token/getLongLivedToken`,
        "facebookMsCheckExpiry": `http://${process.argv[2]}:8080/api/ms/rs/facebook/token/isNearExpiry`,
        "facebookMsExchangeLongLivedAccessToken": `http://${process.argv[2]}:8080/api/ms/rs/facebook/token/exchangeLongLivedAccessToken`,
        "facebookRateLimitStatus": `http://${process.argv[2]}:8080/api/ms/rs/facebook/token/getRateLimitStatus`
      }
envJSON.development.kafka_client.url = 'kafka:2181'
envJSON.development.r_serve_data.hostName="r"
fs.writeFileSync("/code/realsociable/RealSociableSearchEngine/env.json", JSON.stringify(envJSON, null, 2))
data = fs.readFileSync("/code/rsmicroservices/rsAPIMicroservices/env.json");
envJSON = JSON.parse(data);
envJSON.development.kafka_client.url = 'kafka:2181'
fs.writeFileSync("/code/rsmicroservices/rsAPIMicroservices/env.json", JSON.stringify(envJSON, null, 2))
