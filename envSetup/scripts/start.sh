#!/bin/bash
if [[ $ENVN = "development" ]];then
        sed -i -r 's/^(bootstrap\.servers=)(.*$)/\1kafka:9092/g' \
        /code/rsapimicroservices/common/src/main/resources/producer.props
        sed -i -r 's/^(bootstrap\.servers=)(.*$)/\1kafka:9092/g' \
        /code/rsapimicroservices/core/src/main/resources/consumer.props
fi
#rsApp
cd /code/realsociable/RealSociableSearchEngine && \
git pull && \
git checkout $RS_APP_GIT_BRANCH && \
git checkout $RS_APP_SHA && \
npm install && \
(tsc -p . || node /setEnvVariables.js $HOST_IP) && \
cp -R -v /code/realsociable/RealSociableSearchEngine /output/ && \
# rsApp ui
cd /code/realsociableui && \
git pull && \
git checkout $RS_APP_UI_GIT_BRANCH && \
git checkout $RS_APP_UI_SHA && \
npm install && \
cp -R -v /code/realsociableui /output/ && \
# R
cd /code/realsociable/RModelling && \
cp -R -v /code/realsociable/RModelling /output/ && \
#node microservices
cd /code/rsmicroservices/rsAPIMicroservices/ && \
(tsc -p . || npm install) && \
cp -R -v /code/rsmicroservices/rsAPIMicroservices/ /output/ && \
#java microservices
cd /code/rsapimicroservices && \
git pull && \
git checkout $JAVA_MS_GIT_BRANCH && \
git checkout $JAVA_MS_SHA && \
mvn clean install && \
cp -v ./core/target/core-0.0.1-SNAPSHOT.jar /output/javaMS