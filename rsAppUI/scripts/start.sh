#! /bin/bash

if [[ $NODE_ENV = "development" ]]; then
    echo "Starting in development"
    supervisord -n
else
    echo "Starting in production"
    cd /realsociableui
    npm run build:client
    sed -i -r "s/localhost/$HOST_IP/g" /etc/nginx/conf.d/server1.conf
    nginx -g "daemon off;"
fi