#!/bin/bash

# Optional ENV variables:
# * ADVERTISED_HOST: the external ip for the container, e.g. `docker-machine ip \`docker-machine active\``
# * ADVERTISED_PORT: the external port for Kafka, e.g. 9092
# * ZK_CHROOT: the zookeeper chroot that's used by Kafka (without / prefix), e.g. "kafka"
# * LOG_RETENTION_HOURS: the minimum age of a log file in hours to be eligible for deletion (default is 168, for 1 week)
# * LOG_RETENTION_BYTES: configure the size at which segments are pruned from the log, (default is 1073741824, for 1GB)
# * NUM_PARTITIONS: configure the default number of log partitions per topic

# Configure advertised host/port if we run in helios

#remove .lock file
rm /tmp/kafka-logs/.lock
# ifconfig eth0 is of the form.
# eth0      Link encap:Ethernet  HWaddr 02:42:ac:14:00:02  
#           inet addr:172.20.0.2  Bcast:0.0.0.0  Mask:255.255.0.0
#           inet6 addr: fe80::42:acff:fe14:2/64 Scope:Link
#           UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
#           RX packets:93 errors:0 dropped:0 overruns:0 frame:0
#           TX packets:13 errors:0 dropped:0 overruns:0 carrier:0
#           collisions:0 txqueuelen:0 
#           RX bytes:14230 (13.8 KiB)  TX bytes:1026 (1.0 KiB)
# grep inet addr to get
#           inet addr:172.20.0.2  Bcast:0.0.0.0  Mask:255.255.0.0
# then select the second column with awk.to get
# addr:172.20.0.2
# then match addr:172.20.0.2 with regex using sed and get the ip
#P.S: pls improve upon it.awk should be enough ideally.
ADVERTISED_HOST=$(ifconfig eth0| grep "inet addr"| awk '{print $2}'|sed 's/.*:\(.*\).*/\1/g')

# Set the external host and port
echo "advertised host: $ADVERTISED_HOST"
sed -i "s/\(advertised.host.name=\)\(.*$\)/\1$ADVERTISED_HOST/" $KAFKA_HOME/config/server.properties

if [ ! -z "$ADVERTISED_PORT" ]; then
    echo "advertised port: $ADVERTISED_PORT"
    sed -r -i "s/#(advertised.port)=(.*)/\1=$ADVERTISED_PORT/g" $KAFKA_HOME/config/server.properties
fi


# Run Kafka
$KAFKA_HOME/bin/kafka-server-start.sh $KAFKA_HOME/config/server.properties
